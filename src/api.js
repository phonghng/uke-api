const express = require("express");
const serverless = require("serverless-http");
const cors = require('cors')
const uke = require("phonghng-url-kw-extractor");

const app = express();
const router = express.Router();

router.get('/', cors(corsOptions), function(req, res, next) {
  res.writeHead(302, {
    Location: "https://www.npmjs.com/package/phonghng-url-kw-extractor"
  });
  res.end();
});

router.get('/supportlang', cors(corsOptions), function(req, res, next) {
  res.json({ "support-lang": uke.support_lang });
}); 

router.post('/kwurl', cors(corsOptions), function(req, res, next) {
  uke.get_keyword(req.body.url, keywords => {
    res.json(keywords);
  });
});

router.post('/kwhtml', cors(corsOptions), function(req, res, next) {
  uke.get_keyword_from_html(req.body.html, keywords => {
    res.json(keywords);
  });
});

router.post('/kwstring', cors(corsOptions), function(req, res, next) {
  uke.get_keyword_from_string(req.body.string, keywords => {
    res.json({ "keywords": keywords });
  });
});

app.use(`/.netlify/functions/api`, router);

module.exports = app;
module.exports.handler = serverless(app);